import React, { Component } from 'react';
import axios from 'axios';
import Swipeable from 'react-swipeable';
import './App.css';

import Card from "./Components/Card";

class App extends Component {
  constructor() {
    super();

    this.state = {
      messages: [],
      loading: false,
      paginationToken: null
    };

    this.getMessages();

    this.rightSwiped = this.rightSwiped.bind(this);
  }

  componentDidMount() {
    const self = this;
    this.refs.iScroll.addEventListener("scroll", () => {
      if (!self.state.loading && (this.refs.iScroll.scrollTop + this.refs.iScroll.clientHeight >= (this.refs.iScroll.scrollHeight - 50))) {
        self.getMessages();
      }
    });
  }

  render() {
    return (
      <div className="App">
        <header className="app-header">
          <div className="icon-container">
            <svg viewBox="0 0 50 50" version="1.1" width="50px" height="50px" className="menu-icon">
              <g id="surface1">
                <path d="M 0 9 L 0 11 L 50 11 L 50 9 Z M 0 24 L 0 26 L 50 26 L 50 24 Z M 0 39 L 0 41 L 50 41 L 50 39 Z "/>
              </g>
            </svg>
          </div>
          <div className="app-title">Messages</div>
        </header>
        <div className="contentContainer" ref="iScroll">
          <div className="content" >
            {this.state.messages.map((message, index) => {
              return (
                <Swipeable
                  onSwipedRight={this.rightSwiped}
                >
                  {this.renderCard(message, index)}
                </Swipeable>
              );
            })}
          </div>
        </div>
      </div>
    );
  }

  renderCard(message, index) {
    return <Card index={index} author={message.author.name} content={message.content} photoUrl={message.author.photoUrl} time={message.updated}/>;
  }

  rightSwiped(evt, deltaX, isFlick) {
    if (evt.target && isFlick) {
      let el = evt.currentTarget.children[0];
      const index = el.getAttribute('data-index');
      if (el && index) {
        const msgArr = this.state.messages;
        msgArr.splice(index, 1);
        this.setState({messages: msgArr});
      }
    }
  }

  getMessages() {
    const self = this;
    this.setState({loading: true});
    let urlPath = "http://message-list.appspot.com/messages";
    if (this.state.paginationToken) {
      urlPath += '?pageToken=' + this.state.paginationToken;
    }
    const requestParams = {
      method: 'GET',
      url: urlPath,
      headers: {
        'Content-Type': 'application/json'
      }
    };
    axios(requestParams).then((response) => {
      const currentList = self.state.messages;
      self.setState({messages: currentList.concat(response.data.messages), paginationToken: response.data.pageToken});
      self.setState({loading: false});
    });
  }
}

export default App;
