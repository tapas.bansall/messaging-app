import React, {Component} from 'react';
import moment from 'moment';

import './Card.css';


class Card extends Component {
	render() {
		const photoPath = "http://message-list.appspot.com" + this.props.photoUrl;
		return (
			<div className="cardContainer" data-index={this.props.index}>
				<div className="cardHeader">
					<img src={photoPath} className="cardIcon" alt=""/>
					<div className="cardTitleContainer">
						<div className="cardTitle">{this.props.author}</div>
						<div className="cardSubtitle">{this.getTimeDiff(this.props.time)} ago</div>
					</div>
				</div>
				<div className="cardContent">
					{this.props.content}
				</div>
			</div>
		);
	}

	getTimeDiff(date) {
		const timeDiff = moment.duration(moment().diff(moment(date)));
		if (timeDiff.years() > 0) {
			return timeDiff.years() + ' years';
		} else if (timeDiff.months() > 0) {
			return timeDiff.months() + ' months';
		} else if (timeDiff.weeks() > 0) {
			return timeDiff.weeks() + ' weeks';
		} else if (timeDiff.days() > 0) {
			return timeDiff.days() + ' days';
		} else if (timeDiff.hours() > 0) {
			return timeDiff.hours() + ' hours';
		} else if (timeDiff.minutes() > 0) {
			return timeDiff.minutes() + ' minutes';
		}
		return timeDiff.seconds() + ' seconds';
	}
}

export default Card;